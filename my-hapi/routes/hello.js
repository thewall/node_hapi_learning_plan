const {jwtHeaderDefine} = require('../utils/router-helper')
module.exports = [
    {
        method:'GET',
        path:'/',
        handler:(req,res)=>{
            res('hello')
            console.log(req.auth.credentials);
        },
        config:{
            tags:['api','testes'],
            description:'测试hello',
            validate:{
                ...jwtHeaderDefine
            }
        }
    }
]