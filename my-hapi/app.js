const Hapi = require('hapi');
// 先引入环境相关配置
require('env2')('./.env');
const config = require('./config');

const hapiAuthJWT = require('hapi-auth-jwt2');
const routesHello = require('./routes/hello');
const routesShop = require('./routes/shop');
const routesOrder= require('./routes/order');
const routesUser = require('./routes/user')

const pluginHapiSwagger = require('./plugins/hapi-swagger')
const pluginHapiAuthJWT = require('./plugins/hapi-auth-jwt')

const server = new Hapi.Server()
server.connection({
    port: config.port,
    host: config.host,
});
// 创建一个接口
const init = async () => {
    await server.register([
        ...pluginHapiSwagger,
        hapiAuthJWT
    ])
    pluginHapiAuthJWT(server)
    server.route([
        ...routesHello,
        ...routesOrder,
        ...routesShop,
        ...routesUser
    ])
    // 启动服务
    await server.start();
    console.log(`server running at:${server.info.uri}`);
}

init();


