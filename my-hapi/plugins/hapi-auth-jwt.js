const config = require('../config')

const validate = (decode, req, cb) => {
    let error;
    /*
    接口 POST /users/createJWT 中的 jwt 签发规则

    const payload = {
      userId: jwtInfo.userId,
      exp: Math.floor(new Date().getTime() / 1000) + 7 * 24 * 60 * 60,
    };
    return JWT.sign(payload, process.env.JWT_SECRET);
  */
    // decode wei jwt payload 解码后数据
    const { userId } = decode;
    if (!userId) {
        return cb(error, false, userId)
    }
    const credentials = {
        userId
    }
    // 在路由接口的 hanlder 中的  req.auth.credentials 中获取 decode 
    return cb(error, true, credentials)
}

module.exports = (server) => {
    server.auth.strategy('jwt', 'jwt', {
        key: config.jwtSecret,
        verifyFunc: validate
    })
    server.auth.default('jwt')
}